set termguicolors
set fillchars+=diff:╱

let g:polyglot_disabled = ['autoindent']
let g:sneak#use_ic_scs = 1

" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
Plug 'tpope/vim-surround'
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'justinmk/vim-sneak'
Plug 'https://github.com/machakann/vim-highlightedyank'
Plug 'https://github.com/lambdalisue/suda.vim'
" Plug 'joshdick/onedark.vim'
Plug 'ayu-theme/ayu-vim'
let ayucolor="dark"   " for dark version of theme
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'https://github.com/shumphrey/fugitive-gitlab.vim'
Plug 'https://github.com/henrik/vim-yaml-helper'
let g:fugitive_gitlab_domains = ['https://abgit2.abinitio.com']
let g:csv_no_conceal = 1

" Svelte stuff
Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'
Plug 'evanleck/vim-svelte', {'branch': 'main'}

" Gleam
Plug 'gleam-lang/gleam.vim'

if has("nvim")
  " Better search
  Plug 'kevinhwang91/nvim-hlslens'
  " Used by many plugins
  Plug 'nvim-lua/plenary.nvim'

  " Treesitter for better syntax highlighting
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

  " Orgmode
  Plug 'nvim-orgmode/orgmode'

  " Rainbow paren matching
  " Plug 'https://github.com/p00f/nvim-ts-rainbow'

	" Easy installation for languages
	Plug 'williamboman/nvim-lsp-installer'

  " Language support
  Plug 'neovim/nvim-lspconfig'

  " Auto-paren pairs
  Plug 'https://github.com/windwp/nvim-autopairs'

  " For git diff in the gutters
	Plug 'lewis6991/gitsigns.nvim'

  " Smooth scrolling
  Plug 'karb94/neoscroll.nvim'

  " Fancy icons in the tree and elsewhere
  Plug 'kyazdani42/nvim-web-devicons'

  " For the fancy tab look of the buffers at the top
  Plug 'akinsho/bufferline.nvim'

  " File finding (ctrl-p)
  Plug 'nvim-telescope/telescope.nvim'

  " Autocomplete
  Plug 'hrsh7th/nvim-cmp'
  Plug 'hrsh7th/cmp-nvim-lsp'
  Plug 'saadparwaiz1/cmp_luasnip'
  Plug 'L3MON4D3/LuaSnip'

  " Fancy statusline at bottom
  Plug 'feline-nvim/feline.nvim'

  " File browser with ctrl+n
  Plug 'kyazdani42/nvim-tree.lua'

  " Alternative paren matching since the default is too slow
  " Plug 'andymass/vim-matchup'

	" Diff view
	Plug 'sindrets/diffview.nvim'
	" Marks in the gutter
	Plug 'chentoast/marks.nvim'

	" For ansible
	Plug 'pearofducks/ansible-vim'

	" Fancy notifications
	" Plug 'rcarriga/nvim-notify'

	" Project management
	Plug 'ahmedkhalf/project.nvim'
	Plug 'lukas-reineke/indent-blankline.nvim'
endif
call plug#end()

" Use space as the leader
nnoremap <SPACE> <Nop>
let mapleader=" "

let g:matchup_matchparen_deferred = 1

if has("nvim")
  let g:onedark_config = {
      \ 'style': 'darker',
  \}
  " colorscheme onedark
	colorscheme ayu

	lua require('gitsigns').setup()
	if !exists("g:neovide")
		lua require('neoscroll').setup()
	endif
  lua require('bufferline').setup{}
  lua require('feline').setup()
  lua require('nvim-tree').setup{}
  lua require('nvim-autopairs').setup{}
	lua require('diffview').setup{}
	lua require('nvim-lsp-installer').setup()
	lua require('marks').setup()
	lua require("indent_blankline").setup { show_end_of_line = true, }

  lua << EOF
require("project_nvim").setup()
-- vim.notify = require("notify")
require('telescope').setup({
  defaults = {
    layout_config = {
      vertical = { width = 0.5 }
      -- other layout configuration here
    },
    -- other defaults configuration here
  },
  -- other configuration values here
})
require('telescope').load_extension('projects')

local opts = { noremap=true, silent=true }
-- Mappings for lsp error checking
-- vim.api.nvim_set_keymap('n', '<Leader>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
vim.api.nvim_set_keymap('n', '<Leader>k', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
vim.api.nvim_set_keymap('n', '<Leader>j', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
vim.api.nvim_set_keymap('n', '<Leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)

local on_attach = function(client, bufnr)
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>e', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>.', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

local lspconfig = require('lspconfig')

local servers = {'rust_analyzer', 'pyright', 'tsserver', 'tailwindcss', 'yamlls', 'ansiblels', 'svelte', 'cssls', 'gleam'}
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
		settings = {
			["rust-analyzer"] = {
				cargo = {
					runBuildScripts = true
				}
			}
		}
	}
end

local luasnip = require 'luasnip'

-- Auto-completion setup
local cmp = require 'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
--    ['<Tab>'] = cmp.mapping(function(fallback)
--      if cmp.visible() then
       -- cmp.select_next_item()
--      elseif luasnip.expand_or_jumpable() then
--        luasnip.expand_or_jump()
--      else
--        fallback()
--      end
--    end, { 'i', 's' }),
--    ['<S-Tab>'] = cmp.mapping(function(fallback)
--      if cmp.visible() then
--        cmp.select_prev_item()
--      elseif luasnip.jumpable(-1) then
--        luasnip.jump(-1)
--      else
--        fallback()
--      end
--    end, { 'i', 's' }),
  }),
  sources = {
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
    { name = 'orgmode' },
  },
}

-- If you want insert `(` after select function or method item
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
local cmp = require('cmp')
cmp.event:on( 'confirm_done', cmp_autopairs.on_confirm_done({  map_char = { tex = '' } }))

-- Rainbow brackets setup
require("nvim-treesitter.configs").setup {
	auto_install = true,

  highlight = {
		enable = true,
  },

	indent = {
		enable = true,
	},

  incremental_selection = {
    enable = true,
    keymaps = {
			init_selection = "<CR>",
			node_incremental = "<CR>",
			node_decremental = "<BS>",
    },
  },

  rainbow = {
    enable = true,
    extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- Do not enable for files with more than n lines, int
  }
}


-- https://github.com/nvim-telescope/telescope.nvim/issues/1277
vim.api.nvim_create_autocmd({ "BufEnter" }, {
    pattern = { "*" },
    command = "normal zx",
})
vim.diagnostic.config({signs = false}) -- don't show diagnostics in the gutter

require('hlslens').setup({
    calm_down = true,
    nearest_only = true
})

local kopts = {noremap = true, silent = true}

vim.api.nvim_set_keymap('n', 'n',
    [[<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>]],
    kopts)
vim.api.nvim_set_keymap('n', 'N',
    [[<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>]],
    kopts)
vim.api.nvim_set_keymap('n', '*', [[*<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', '#', [[#<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', 'g*', [[g*<Cmd>lua require('hlslens').start()<CR>]], kopts)
vim.api.nvim_set_keymap('n', 'g#', [[g#<Cmd>lua require('hlslens').start()<CR>]], kopts)

vim.api.nvim_set_keymap('n', '<Leader>l', ':noh<CR>', kopts)


-- init.lua

-- Load custom treesitter grammar for org filetype
require('orgmode').setup_ts_grammar()

-- Treesitter configuration
require('nvim-treesitter.configs').setup {
  -- If TS highlights are not enabled at all, or disabled via `disable` prop,
  -- highlighting will fallback to default Vim syntax highlighting
  highlight = {
    enable = true,
    -- Required for spellcheck, some LaTex highlights and
    -- code block highlights that do not have ts grammar
    additional_vim_regex_highlighting = {'org'},
  },
  ensure_installed = {'org'}, -- Or run :TSUpdate org
}

require('orgmode').setup({
  org_agenda_files = {'/disk1/org/**/*'},
  org_default_notes_file = '/disk1/sand/main.org',
})
EOF
endif


" Don't use Ex mode, use Q for formatting.
map Q gq
syntax on " highlight syntax
set number " show line numbers
set expandtab " Use only spaces, never tabs
set nofixeol " Don't add a newline for no reason at all
set linebreak " don't wrap in the middle of a word
" set noswapfile " disable the swapfile
" set nohlsearch " do not highlight search
set ignorecase " ignore case in search
set smartcase " case sensitive only when upper case
set foldmethod=syntax " do line folding according to syntax
set foldlevelstart=99 " start with entire file unfolded
set cursorline " highlight the current line
set scrolloff=5 " 5 line scroll padding
set incsearch " do incremental searching.
set clipboard=unnamedplus " use system clipboard
set number relativenumber " use relative line numbers
set signcolumn=yes:2 " keep the signs column always there so there's no jump with gitsigns. 1 for gitsigns + 1 for marks

if has("nvim")
	set foldmethod=expr
	set foldexpr=nvim_treesitter#foldexpr()
  nnoremap <C-n> :NvimTreeToggle<CR>
endif

" Use ht as escape
inoremap ht <ESC>
xnoremap ht <ESC>

" Now remap hh to h to avoid the pause after typing h
inoremap hh h
xnoremap hh h


" Set cursor size to be skinny in insert mode
let &t_SI = "\<Esc>[6 q"
let &t_EI = "\<Esc>[2 q"


" Keep selection after indent/unindent
vnoremap > >gv
vnoremap < <gv

" Use ctrl-j to break the line under the cursor
nmap <c-j> i<CR><Esc>l

" Don't highlight sneak
highlight Sneak guifg=black guibg=yellow ctermfg=black ctermbg=yellow

" Move lines up and down
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" Toggle relative line numbers with space-n
map <leader>n :set rnu!<CR>
" Toggle English spellcheck with space-e
" map <leader>e :setlocal spell! spelllang=en_us<CR>


" Make w!! save with sudo
cmap w!! SudaWrite

" Toggle background transparency with <leader> t
let t:is_transparent = 0
function! Toggle_transparent_background()                      
  if t:is_transparent == 1
    hi Normal guibg=#111111 ctermbg=235                     
    hi SignColumn guibg=#111111 ctermbg=235                     
    let t:is_transparent = 0
		let g:neovide_transparency=1
  else
    hi Normal guibg=NONE ctermbg=NONE
    hi SignColumn guibg=NONE ctermbg=NONE
    let t:is_transparent = 1
		let g:neovide_transparency=0.75
  endif                    
endfunction               
nnoremap <silent><leader>t :call Toggle_transparent_background()<CR>

" Toggle neovide fullscreen with <leader> f
let g:neovide_fullscreen=v:false

function! Toggle_fullscreen_neovide()                      
  if g:neovide_fullscreen == v:true
		let g:neovide_fullscreen=v:false
  else
		let g:neovide_fullscreen=v:true
  endif                    
endfunction               
nnoremap <silent><leader>F :call Toggle_fullscreen_neovide()<CR>

" <leader> f to detect filetype for syntax highlighting
" nnoremap <leader>f :filetype detect<CR>

nnoremap <Leader>w :w<CR>
nnoremap <Leader>a :wa<CR>
nnoremap <Leader>q :wq<CR>
nnoremap <Leader>! :q!<CR>

nnoremap <Leader>x :bd<CR>
nnoremap <Leader>X :bd!<CR>

nmap <Leader>i <cmd>:IndentBlanklineToggle<cr>

" Toggle git blame on <leader>b
nmap <expr> <Leader>b &filetype == 'fugitiveblame' ? ":q<CR>" : ":Git blame<CR>"

" These commands will navigate through buffers in order regardless of which mode you are using
" e.g. if you change the order of buffers :bnext and :bprevious will not respect the custom ordering
nnoremap <Leader>l :BufferLineCycleNext<CR>
nnoremap <Leader>h :BufferLineCyclePrev<CR>

" These commands will move the current buffer backwards or forwards in the bufferline
nnoremap <Leader>L :BufferLineMoveNext<CR>
nnoremap <Leader>H :BufferLineMovePrev<CR>

" Find files using Telescope command-line sugar.
nnoremap <c-p> <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>fj <cmd>Telescope jumplist<cr>
nnoremap <leader>fm <cmd>Telescope marks<cr>
nnoremap <a-p> <cmd>Telescope projects<cr>
nnoremap <leader>N :!neovide<cr>

" YAML helpers
nnoremap <leader>yp :YamlP<cr>
nnoremap <leader>yf :YamlF<cr>

autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()

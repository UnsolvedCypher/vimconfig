#!/bin/bash

ln -sf $(pwd)/.vimrc ~/.vimrc &&
ln -sf $(pwd)/.ideavimrc ~/.ideavimrc &&
mkdir -p ~/.config/nvim &&
ln -sf $(pwd)/.vimrc ~/.config/nvim/init.vim;
vim +PlugInstall +qall;
nvim +PlugInstall +qall;
